#!/bin/bash

sitesAvailable=/etc/nginx/sites-available
sitesEnabled=/etc/nginx/sites-enabled

# Project name
echo -ne "Enter the name of project \n"
read projectName;

# if not empty or 'enter' key 
if [[ "$projectName" != "" ]]    
then
  # go to path
  cd $sitesAvailable
  
  # create and write config
  touch $sitesAvailable/$projectName 
{
echo "server {
        listen 80; 
        server_name    ${projectName};
        root  ${HOME}/projects/${projectName};"

echo '        index index.php index.html;  
        
        location ~* \.(jpg|jpeg|gif|css|png|js|ico|html)$ {
                access_log off;
                expires max;
                log_not_found off;
        }

        location / {                
                try_files $uri $uri/ /index.php?$query_string;
        }

        location ~* \.php$ {
            try_files $uri = 404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }

        location ~ /\.ht {
            deny all;
        }
}'
} > $sitesAvailable/$projectName

# create link
ln -s $sitesAvailable/$projectName $sitesEnabled

# make copy of the hosts
cp /etc/hosts /etc/hosts_old

# create new record in hosts and add old records to the end of file
echo "127.0.0.1   ${projectName}" > /etc/hosts; cat /etc/hosts_old >> /etc/hosts

# restart nginx
service nginx restart
else # if empty string or key 'enter' exit with error
	exit 1;
fi

read -p "Done"
