# Automatic creation of configs for projets on nginx

**_!! Before start use this script make sure that you make backup of file hosts_**

## Usage

* In the file "new_project.desktop" change [path to script](http://prntscr.com/qyvky5) to your home directory
* Move file "new_project.desktop" to your desktop or whatever you want
* Then move file "generate_project.sh" to your home directory
* Create folder 'projects' in your home directory 
* After that just click on "new project"
* * Enter your pass
* * Enter the name of project (your directory) without spaces
* * Thats all, script will create config in etc/nginx/sites-available/your_project_name, than will make a link in etc/nginx/sites-enabled, will change file hosts and then restart nginx

